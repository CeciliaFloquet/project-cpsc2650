import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';
// import assert from 'assert';

describe('\'user\' service', () => {

    let mongoServer;
    let app;
    const usersInfo = {
        firstName:'first',
        lastName:'last',
        email: 'firstLast@example.com',
        username:"firstLast",
        password: 'secret',
        phone: '+16666666666'
    }; 
    const usersInfo2 = {
        firstName:'first',
        lastName:'last',
        email: 'firstLast2@example.com',
        username:"firstLast2",
        password: 'secret',
        phone: '+16666666666'
    };
    beforeAll(async () => {
        mongoServer = new MongoMemoryServer();
        process.env.MONGODBURI = await mongoServer.getUri();
        process.env.RECAPTCHA_SECRET = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
        app = appFunc();
    });

    afterAll(async () => {
        await mongoServer.stop();
    });

    it('registered the service', () => {
        const service = app.service('users');
        expect(service).toBeTruthy();
    });
      
    it('creates user', async () => {
        const user = await app.service('users').create(usersInfo);
        expect(user).toBeTruthy();
    });
    // it('creates duplicate user', async () => {
    //    const user = await app.service('users').create(usersInfo);
    //     expect(response.data.message).toBe('Page not found');
    // });
    //create user and check password
    it('creates user and check if password is correct', async () => {
        const user =await app.service('users').create(usersInfo2);
        expect(user.password ).not.toBe('secret2');
        expect(user.password ).toBe('secret');
    });

     it('creates user recaptcha fails', async () => {
         process.env.RECAPTCHA_SECRET = "";
        await expect( async () => {
            const user = await app.service('users').create(usersInfo);
        }).rejects.toThrow();
    });


});
