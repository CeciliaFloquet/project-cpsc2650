import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'categories\' service', () => {

    let mongoServer;
    let app;
    let user;
    let id;
     const usersInfo = {
        firstName:'first',
        lastName:'last',
        email: 'firstLast@example.com',
        username:"firstLast",
        password: 'secret',
        phone: '+16666666666'
    }; 
    beforeAll(async () => {
        mongoServer = new MongoMemoryServer();
        process.env.MONGODBURI = await mongoServer.getUri();
        app = appFunc();
        process.env.RECAPTCHA_SECRET = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
        user = await app.service('users').create(usersInfo);
        app.service('users').find({
            username:"firstLast"
        }).then((result)=>{
            id= result.data[0]._id
        })
    });

    afterAll(async () => {
        await mongoServer.stop();
    });

    const usersCategory= {
        name:'category',
        user:id
    }; 
    const CategoryFail= {
        name:'category'
    }; 

    it('registered the service', () => {
        const service = app.service('categories');
        expect(service).toBeTruthy();
    });
    it('creates category', async () => {
        const category = await app.service('categories').create(usersCategory);
        expect(category).toBeTruthy();
    });
  

});
