import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'transactions\' service', () => {

    let mongoServer;
    let app;
    let id;
    let user;
    const usersInfo = {
        firstName:'first',
        lastName:'last',
        email: 'firstLast@example.com',
        username:"firstLast",
        password: 'secret',
        phone: '+16666666666'
    };
    beforeAll(async () => {
        mongoServer = new MongoMemoryServer();
        process.env.MONGODBURI = await mongoServer.getUri();
        app = appFunc();
        process.env.RECAPTCHA_SECRET = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
        user = await app.service('users').create(usersInfo);
        app.service('users').find({
            username:"firstLast"
        }).then((result)=>{
            id= result.data[0]._id
        })

    });

    afterAll(async () => {
        await mongoServer.stop();
    });

    it('registered the service', () => {
        const service = app.service('transactions');
        expect(service).toBeTruthy();
    });
    const userstransactions= {
        value:10,
        category:'category',
        user:id,
        description:'description'
    }; 
    it('creates transaction', async () => {
        const transaction = await app.service('transactions').create(userstransactions);
        expect(transaction).toBeTruthy();
    });

});
