import { Application } from '../declarations';


import users from './users/users.service';


import categories from './categories/categories.service';


import transactions from './transactions/transactions.service';


import sms from './sms/sms.service';


// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(users);
  app.configure(categories);
  app.configure(transactions);
  app.configure(sms);
}
