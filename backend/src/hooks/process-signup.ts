// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

import querystring from 'querystring';
import axios from 'axios';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
    return async (context: HookContext): Promise<HookContext> => {
  
        const { data } = context;

        console.log( data );
        
        // verify the incoming token against the reCAPTCHA service
        const response = await axios.post(
        'https://www.google.com/recaptcha/api/siteverify',
        querystring.stringify({
            secret: process.env.RECAPTCHA_SECRET,
            response: data.token
        })
        );
    
        console.log(response.data);
        
        // if the response fails or the score is too low, throw an error
        if ( !response.data.success || response.data.score < 0.7 ) {
        throw new Error('reCAPTCHA fail');      
        }
        
        // if everything is OK, carry on
        return context;
  };
};
