// import React from 'react';
import client from './feathers';
import ReactDOM from 'react-dom';
import FinancialPage from './financialPage';


import React from 'react';




class SignIn extends React.Component { 
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            password:"",
            id:"", 
            firstName: ""
        };
      
        this.checkName = this.checkName.bind(this);
        this.checkPassword = this.checkPassword.bind(this);
        this.checkUser = this.checkUser.bind(this);
        this.financialPage = this.financialPage.bind(this);


     
    }
   
   
    financialPage(){
          let financialProps={
            id: this.state.id,
            firstName:this.state.firstName           
        };
        
        ReactDOM.render(<FinancialPage   {...financialProps} />, document.getElementById('app'));
    }
    checkName(event){
        this.setState({       
            name:event.target.value
        });
    }
    checkPassword(event){
        this.setState({       
            password:event.target.value
        });
    }
    checkUser(event){
        event.preventDefault();
            this.state.users.find({
                 query:{
                    username:this.state.name, 
                    password: this.state.password
                 }
            
                })
                 .then((results) => {
                    // console.log(JSON.stringify(results.data));
                    if(results.data === '' || results.data === null) {
                        alert("This user not exist, wrong password or wrong user name");
                    }else{
                        console.log("tttt" +results.data[0].firstName);
                        this.setState({       
                            id:results.data[0]._id,
                            firstName:results.data[0].firstName
                            });
                        this.financialPage();
                    }
                }).catch(error=>console.log(error));

    }
     componentDidMount() {
        const users = client.service('users');

        this.setState({users})



    }
   
    render() {     
        return <div className="container2"> 
                    <h2>Login</h2>
                    <form onSubmit={this.checkUser}> 
                        <label ><b>Username</b></label>
                        <input type="text" placeholder="Enter Username" name="username" onChange={this.checkName} required/>
                        <label ><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="password" onChange={this.checkPassword} required/>
                        <button type="submit">Login</button>
                    </form>
                     
                    
                </div>;
    } 
} 
export default SignIn; 
