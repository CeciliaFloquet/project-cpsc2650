import React from 'react'; 

class ShowTransactions extends React.Component { 
  
   constructor(props) {
        super(props);
       this.displayFilter = this.displayFilter.bind(this);
    }
   displayFilter(event){
     
        this.props.filterWord(event.target.value);
   }
   
  componentDidMount() {
        this.props.updateCategory();
        this.props.updateTransactions();        
    }
   
    render() { 
        let style={
            color: `${this.props.color}`
            
        };
        
        return <div className="showTransactions"> 
                    <div className="balance">
                        <h1>Balance: {this.changeChangeColor}</h1>
                        <h1  style={style}> {this.props.balance}</h1>
                    </div>
                    <div className="showTransaction">
                        <h1 className="filterCategoryTitle">Filter for Category</h1>
                        <input type="text"  className="filterCategory" placeholder="Search for category" onChange={(event)=>this.displayFilter(event)}/>
                    </div>
                    <div className="listTransaction">
                        <table>
                            <tbody>
                                 <tr>
                                <th>Transaction Category</th>
                                <th>Transaction Description</th>
                                <th>Value</th>
                            </tr>
                           {this.props.transactions && this.props.transactions.map(trans=>
                                <tr key={trans._id}>
                                    <td>{trans.category}</td>
                                    <td>{trans.description}</td>
                                    <td>{trans.value}</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </div> 
                </div>;
    } 
} 
export default ShowTransactions;