import React from 'react'; 
import client from './feathers';
import CreateCategory from './createCategory.js'; 
import CreateTransactions from './createTransactions.js'; 
import ShowTransactions from './showTransactions.js';

import Application from './application';
import ReactDOM from 'react-dom';

class FinancialPage extends React.Component { 
  
   constructor(props) {
        super(props);
        this.state = {
            category:[],
            id:this.props.id,
            transactions:[],
            filWord:"ALL",
            balance:0,
            color:"blue",
            firstName: this.props.firstName
        };
        this.updateCategory = this.updateCategory.bind(this);
        this.updateTransactions = this.updateTransactions.bind(this);
        this.filterWord=this.filterWord.bind(this);
         this.signoutPage=this.signoutPage.bind(this);
    }
    componentDidMount() {
        const users = client.service('users');
        const categoriesService = client.service('categories');
        const transactionService = client.service('transactions');
        const sms = client.service('sms');
        this.setState({categoriesService, transactionService, users, sms,  firstName: this.props.firstName})
    }
    updateCategory() {
    const categoriesService = client.service('categories');

       categoriesService.find({
            query:{
                user:this.state.id
            }
        })
        .then(result=>{
            
            this.setState({
                category:result.data
            });
        })
        .catch(error=>console.log(error));    
    }
    updateTransactions(){
    const transactionService = client.service('transactions');
       
        if(this.state.filWord==="ALL"){
            transactionService.find({
                query:{
                    user:this.state.id
                    
                }
            })
            .then(result=>{
                
                var temp=0;
                this.setState({
                    transactions:result.data
                });
                this.state.transactions.map(trans=>
                    temp= temp + trans.value
                );
                this.setState({
                    balance:temp
                });
                if(this.state.balance < 0){
                    this.setState({
                        color:"red"
                    });
                    this.state.users.find({
                        query:{
                            _id:this.state.id
                        }
            
                    })
                    .then((results) => {
                        // console.log(JSON.stringify(results.data));
                        if(results.data === '' || results.data === null) {
                            alert("This user not exist, wrong password or wrong user name");
                        }else{
                            const to = results.data[0].phone
                            const msg = "You account has a value of $"  + this.state.balance;

                            this.state.sms.create({
                                to,
                                msg

                            })
                            .then(() => {
                              
								to="";
								msg="";
                            });
    
                                
                            
                        }
                    }).catch(error=>console.log(error));

                }else{
                    this.setState({
                        color:"blue"
                    });
                }
            })
            .catch(error=>console.log(error)); 
        }else{

            transactionService.find({
                query:{
                    user:this.state.id,
                    category:this.state.filWord
                    
                }
            })
            .then(result=>{
                
                var temp=0;
                this.setState({
                    transactions:result.data
                });
                this.state.transactions.map(trans=>
                    temp= temp + trans.value
                );
                this.setState({
                    balance:temp
                });
                if(this.state.balance < 0){
                    this.setState({
                        color:"red"
                    });
                }else{
                    this.setState({
                        color:"blue"
                    });
                }
            })
            .catch(error=>console.log(error)); 

        }
    }
    filterWord(text){
        
        if(text==="") text="ALL";
        this.setState({filWord: text}, () => {
            this.updateTransactions();
        });
    }
    signoutPage() {
    
        ReactDOM.render(<Application />, document.getElementById('app'));
     
    }
    render() { 
        let financialControlProps={
            id: this.state.id,
            category:this.state.category,
            balance: this.state.balance,
            transactions:this.state.transactions,
            color:this.state.color,
            updateCategory: this.updateCategory,
            updateTransactions: this.updateTransactions,
            filterWord: this.filterWord,
           
        };
        
        return <div className="financial">
                    <div className="hello">
                        <h1> Hello {this.props.firstName}! </h1>
                    </div>
                    <h1  className="financialControlTitle">Financial Plan</h1>
                    <button  className="signOut" onClick={this.signoutPage}>Sign Out</button> 
                    <div className="financialControl"> 
                       <CreateCategory  {...financialControlProps} />
                       <CreateTransactions {...financialControlProps} />
                   </div>
                   <ShowTransactions {...financialControlProps} />
            </div>;
    } 
} 
export default FinancialPage; 