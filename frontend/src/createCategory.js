import React from 'react'; 

import client from './feathers';

class CreateCategory extends React.Component { 
  
   constructor(props) {
        super(props);
        this.state = {
            categoryName:[]
          
     
        };
        this.changeCategoryName = this.changeCategoryName.bind(this);
        this.submitCategory = this.submitCategory.bind(this);
    }
    changeCategoryName(event){
        this.setState({
            categoryName:event.target.value
        });
    }
    submitCategory(event){
        event.preventDefault();
        const categoriesService = client.service('categories');
        categoriesService.create({
                name: this.state.categoryName,
                user: this.props.id,
    
        })
        .then(results=>{
            this.props.updateCategory();           
            this.setState({
                categoryName:''
            });
        })
        .catch(error=> alert(error + "Your category should be between 1 to 30 characters"));
    }
    componentDidMount() {
        
        const categoriesService = client.service('categories');
        
        this.setState({categoriesService})
    }
    render() { 
        
        return <div className="category"> 
                    <h2>New category</h2>
                    <form onSubmit={(event)=>this.submitCategory(event)}>
                        <label>
                            Enter name of new category:
                            <input type="text" value={this.state.categoryName}  name="category" onChange={(event)=>this.changeCategoryName(event)}/>
                        </label>
                        <input type="submit" value="submit category" />
                    </form>
                </div>;
    } 
} 
export default CreateCategory; 