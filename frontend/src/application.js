import React, { Component } from 'react';
import SignIn from './signIn.js';
import ReactDOM from 'react-dom';
import SignUp from './signUp.js';
import { loadReCaptcha } from 'react-recaptcha-google'

import ReactGA from 'react-ga';
import { createBrowserHistory } from 'history';

import {
  //BrowserRouter as Router,
    Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import client from './feathers';



// initialize ReactGA
const trackingId="UA-167582825-1"; // Replace with your Google Analytics tracking ID
ReactGA.initialize(trackingId);

// set up history
const history = createBrowserHistory();

// Initialize google analytics page view tracking
history.listen(location => {
  ReactGA.pageview(location.pathname); // Record a pageview for the given page
});

class Application extends Component {
    constructor(props) {
        super(props);
        this.state={};
        this.signupPage = this.signupPage.bind(this);
        
    }
    signupPage() {
    
        ReactDOM.render(<SignUp />, document.getElementById('app'));
     
    }
    callback(){

    }
    componentDidMount() {

        loadReCaptcha();
    }

    render() {
        return <div className="grid-container">
                    <Router history={history}>
                            <div className="header"> 
                                <h1 className="title"> Financial Planning </h1>
                                   
                                    <button  className="signUp" onClick={this.signupPage} to="/user">Sign Up</button> 
                                   
                                <div  className="style" ></div>
                            </div>
                            <div className="container">
                                <SignIn/>
                            </div>
                            <div  className="back"></div>

                        <Switch>
                            <Route path="/user">
                                <SignUp/> 
                            </Route>
                        </Switch>  

                    </Router>

                </div>
    }
}
export default Application;