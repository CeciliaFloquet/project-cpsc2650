import client from './feathers';
import React from 'react'; 
// import CreateCategory from './CreateCategory.js'; 
// import CreateTransactions from './CreateTransactions.js'; 
// import axios from 'axios';
// import FinancialControllers from './financialPage';

class CreateTransactions extends React.Component { 
  
   constructor(props) {
        super(props);
        this.state = {
            transactionsValue:[],
            categoryName:"",
            id:this.props.id,
            transactionsDescription:"",
            type:""
        }
        this.findCategory = this.findCategory.bind(this);
        this.changetransactionsValue = this.changetransactionsValue.bind(this);
        this.changeCategoryName = this.changeCategoryName.bind(this);
        this.submitTransactions = this.submitTransactions.bind(this);
    }
    findCategory(){
         if(this.props.category != null){
             this.props.category.map(category=><option key={category.name} value={category.name}>{category.name}</option>)
             
         }
    }
    
    changetransactionsValue(event){
        this.setState({
          transactionsValue:event.target.value
        });
    }
    changeCategoryName(event){
        this.setState({
          categoryName:event.target.value
        });
    }
    submitTransactions(event){
        event.preventDefault();
        
        var temp= this.state.transactionsValue;
        if(this.state.type === "Debit"){
           temp ="-" + this.state.transactionsValue;
            
        }
            
        this.state.transactionService.create({
            value:temp,
            category:this.state.categoryName,
            user:this.state.id,
            description:this.state.transactionsDescription
    
        })
        .then(results=>{
           
            this.props.updateTransactions();
        })
        .catch(error=>alert(error + "Enter a valid number in the value campus"))
        
    }
    changetransactionsDescription(event){
          this.setState({
                transactionsDescription:event.target.value
            });
    }
    changeType(event){
        this.setState({
          type:event.target.value
        });
       
    }
    componentDidMount() {
        const transactionService = client.service('transactions');
        this.setState({transactionService})
    this.props.updateCategory();
    this.props.updateTransactions();
  }
    render() { 
        
        return <div>
        
                    <div className='transactions'>
                        <h2>New Transactions</h2>
                        <form onSubmit={(event)=>this.submitTransactions(event)}>
                            <label>
                                Value:
                                <input type="text" value={this.state.transactionsValue} onChange={(event)=>this.changetransactionsValue(event)}/>
                            </label>
                            <label>
                                Description:
                            </label>
                                <input type="text" value={this.state.transactionsDescription} onChange={(event)=>this.changetransactionsDescription(event)}/>
                            <label>
                                Category:
                                <select onChange={event=>this.changeCategoryName(event)}>
                                <option value=''>None</option>
                                {this.props.category && this.props.category.map(category=><option key={category.name} value={category.name}>{category.name}</option>)}
                                </select>
                            </label>
                            <label>
                                Type:
                                <select onChange={event=>this.changeType(event)}>
                                    <option value=''>None</option>
                                    <option value='Credit'>Credit</option>
                                    <option value='Debit'>Debit</option>
                                </select>
                            </label>
                            <input type="submit" value="Submit Transactions" />
                        </form>
                    </div>
                </div>;
    } 
} 
export default CreateTransactions;