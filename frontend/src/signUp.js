import client from './feathers';
import React, { Component } from 'react';
import { ReCaptcha } from 'react-recaptcha-google';
import ReactGA from 'react-ga';
import Application from './application';
import ReactDOM from 'react-dom';

class SignUp extends Component { 
    constructor(props, context) {
        super(props, context);
        this.state = {
            firstName: "",
            lastName: "",
            email:"",
            username:"",
            password:"" ,
            phone:" ",
            token: ""
                   
        };
        this.setFirstName = this.setFirstName.bind(this);
        this.setEmail = this.setEmail.bind(this);
        this.setPassword = this.setPassword.bind(this);
        this.setLastName = this.setLastName.bind(this);
        this.setUsername = this.setUsername.bind(this);
        this.createUser = this.createUser.bind(this);
        this.setPhone = this.setPhone.bind(this);

        this.onLoadRecaptcha = this.onLoadRecaptcha.bind(this);
        this.verifyCallback = this.verifyCallback.bind(this);
    }
    //set phone
    setPhone(event){
        this.setState({       
            phone:event.target.value
        });
    }

    //set fisrtName
    setFirstName(event){
        this.setState({       
            firstName:event.target.value
        });
    }
    //set userName
    setUsername(event){
        this.setState({       
            username:event.target.value
        });
    }
    //set lastName
    setLastName(event){
        this.setState({       
            lastName:event.target.value
        });
    }
    //set email
    setEmail(event){
        this.setState({       
            email:event.target.value
        });
    }
    //set password
    setPassword(event){
        this.setState({       
            password:event.target.value
        });
    }
    //create User
    createUser(event){
        this.state.users.find({
            query:{
                username: this.state.username
            
            }
        }).then((result) => {
            console.log("test");          
            console.log(JSON.stringify(result.data));
            if(result.data[0] == '' || result.data[0] == null || result.data == null){
               
                this.state.users.create({
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                username: this.state.username,
                password: this.state.password,
                phone:this.state.phone,
                token: this.state.token
            }).then((result) => {

                ReactGA.event({
                    category: "user",
                    action: "create",
                });

                this.setState({
                    firstName: " ",
                    email:" ",
                    lastName:"",
                    username:"",
                    password:"",
                    phone:""
                })
				ReactDOM.render(<Application />, document.getElementById('app'));
            }).catch((error)=>{
                console.log("error: " + error);
                console.log("error: " + error.message);
            })
            }else{
                
                alert("this user exist, try another username");
            
            }
        }).catch((error)=>{
            console.log("error: " + error);
            console.log("error: " + error.message);
        })
           
            
            
        event.preventDefault();
    }
    componentDidMount() {
        const users = client.service('users');

        this.setState({users});




        if (this.captchaDemo) {
            console.log("started, just a second...")
            this.captchaDemo.reset();
            this.captchaDemo.execute();
        }
    }

    onLoadRecaptcha() {
        if (this.captchaDemo) {
            this.captchaDemo.reset();
            this.captchaDemo.execute();
        }
    }
    verifyCallback(recaptchaToken) {
        this.setState({       
            token:recaptchaToken
        });
  
    }
 
    render() {  
        
       
        return  <div >
                    <div className="containerSignUp" noValidate > 
                        <h2>Sign Up</h2>
                        <div className="containerForm">

                             <ReCaptcha
                                    ref={(el) => {this.captchaDemo = el;}}
                                    size="invisible"
                                    render="explicit"
                                    sitekey="6LeRALcZAAAAAOuqCfG6Ktzn7zpFsUt_TE1Csdl0"
                                    onloadCallback={this.onLoadRecaptcha}
                                    verifyCallback={this.verifyCallback}
                                />
                            <form onSubmit={this.createUser} > 
                                <div className="form-row">
                                    <div className="col-md-4 mb-3">
                                        <label >First name</label>
                                        <input type="text" className="form-control" id="validationServer01"  value={this.state.firstName} onChange={this.setFirstName}required/>
                                        <div className="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div className="col-md-4 mb-3">
                                        <label >Last name</label>
                                        <input type="text" className="form-control" id="validationServer02"  value={this.state.lastName} onChange={this.setLastName}required/>
                                        <div className="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>
                                    <div className="col-md-4 mb-3">
                                        <label >Username</label>
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text" id="inputGroupPrepend3">@</span>
                                            </div>
                                            <input type="text" className="form-control " id="validationServerUsername" aria-describedby="inputGroupPrepend3" value={this.state.username} onChange={this.setUsername} required/>
                                            <div className="invalid-feedback">
                                                Please choose a username.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div className="form-row">
                               <div className="col-md-4 mb-3">
                                    <label>Email address:</label>
                                    <input type="email" className="form-control"  value={this.state.email} onChange={this.setEmail} aria-describedby="emailHelp" required/>
                                    <div className="invalid-feedback">
                                        Please choose a valid email.
                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                        <label >phone: (+16666666666)</label>
                                        <input type="text" className="form-control"   value={this.state.phone} onChange={this.setPhone} placeholder="+16666666666" maxLength="13" pattern="[+][1][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]{1,12}"required/>
                                        <div className="valid-feedback">
                                            Looks good!
                                        </div>
                                </div>
                            </div>
                                <div className="form-group">
                                    <label >Password</label>
                                    <input type="password" className="form-control" id="exampleInputPassword1" value={this.state.password} onChange={this.setPassword}/>
                                </div>
                                <button className="btn btn-primary btn-lg btn-block" type="submit">Sign Up</button>
                            </form>
                        </div>	
                        <a href="/" >Back to home</a> 	
                    </div>
                    <div className="alert alert-danger d-none" role="alert" id="captchaMessage">
                        Failed reCAPTCHA
                    </div>
                </div>;
    } 
} 
export default SignUp; 